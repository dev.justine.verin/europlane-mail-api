package fr.jv;


import fr.jv.dto.MailDTO;
import io.quarkus.test.junit.QuarkusTest;
import jakarta.ws.rs.core.MediaType;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

@QuarkusTest
class MailResourceTest {

    @Test
    void testWithNoKey(){
        MailDTO mailDTO = new MailDTO();
        mailDTO.setRecipientEmail("dev.justine.verin@gmail.com");
        mailDTO.setSubject("Petit test");
        mailDTO.setContent("Test");
        given().contentType(MediaType.APPLICATION_JSON).body(mailDTO).post("mail").then().statusCode(403);

    }
    @Test
    void testWithUnknownKey(){
        String apiKey = "unknownKey";
        MailDTO mailDTO = new MailDTO();
        mailDTO.setRecipientEmail("dev.justine.verin@gmail.com");
        mailDTO.setSubject("Petit test");
        mailDTO.setContent("Test");
        given().header("API-KEY", apiKey).contentType(MediaType.APPLICATION_JSON).body(mailDTO).post("mail").then().statusCode(404);

    }

    @Test
    void testWithExpiredQuotaKey(){
        String apiKey = "THRBMkpNOXZGeEF6cVpTUWU4SWg3UT09OjFkb2ptTnJsRkJITDNxV1J4bzh3ZE14UWUzdDRML2FMSlpMOW9tck5yV2M9";
        MailDTO mailDTO = new MailDTO();
        mailDTO.setRecipientEmail("dev.justine.verin@gmail.com");
        mailDTO.setSubject("Petit test");
        mailDTO.setContent("Test");
        given().given().header("API-KEY", apiKey).contentType(MediaType.APPLICATION_JSON).body(mailDTO).post("mail").then().statusCode(403);
    }



    @Test
    public void testCustomMailwithDBKey(){
        String apiKey = "NmhrSVlTTWhmTVcyejdtc0tCZXluZz09OnN0RVE5TExRVk1UdjlXc3NzcCtXQ1Z6RUYzb1RLOEU5dGNGbFpYSHJydkE9";
        MailDTO mailDTO = new MailDTO();
        mailDTO.setRecipientEmail("dev.justine.verin@gmail.com");
        mailDTO.setSubject("Petit test");
        mailDTO.setContent("Test");
        given().header("API-KEY", apiKey).contentType(MediaType.APPLICATION_JSON).body(mailDTO).post("mail").then().statusCode(201);

    }



}