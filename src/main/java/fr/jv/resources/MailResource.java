package fr.jv.resources;



import fr.jv.dto.MailDTO;
import fr.jv.restClient.ServiceApikeyRemote;
import fr.jv.security.APIKeyProtected;
import io.quarkus.mailer.Mail;
import io.quarkus.mailer.Mailer;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jdk.jfr.Description;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jboss.resteasy.client.exception.ResteasyWebApplicationException;

import java.util.concurrent.CompletionException;

@Path("mails")
@Description("Mail API endpoint")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)

public class MailResource {

    @Inject
    Mailer mailer;
    @RestClient
    private ServiceApikeyRemote serviceApikeyRemote;

    @POST
    @APIKeyProtected
    public Response sendMail(@HeaderParam("API-KEY") String apiKey, MailDTO mailDTO) {
        if(mailDTO.getRecipientEmail() == null){
            mailDTO.setRecipientEmail("dev.justine.verin@gmail.com");
        }
        Mail mail = Mail.withText(mailDTO.getRecipientEmail(), mailDTO.getSubject(), mailDTO.getContent());

        try {
            mailer.send(mail);
            serviceApikeyRemote.addMailHistory(apiKey, mailDTO);
            return Response.status(200, "mail envoyé").build();

        } catch (IllegalArgumentException exception) {
            return Response.status(Response.Status.BAD_REQUEST).entity("L'adresse mail utilisée est d'un format " +
                    "invalide").build();
        } catch (ResteasyWebApplicationException exception) {
            return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity("Erreur lors de la connexion au " +
                    "service de gestion des clés APIs").build();
        } catch (CompletionException exception) {
            return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity("Erreur lors de la connexion au " +
                    "serveur SMTP").build();
        }
        catch(NullPointerException exception){
            return Response.status(Response.Status.BAD_REQUEST).entity("Pas d'adresse mail associée à ce compte!").build();

        }
    }


}