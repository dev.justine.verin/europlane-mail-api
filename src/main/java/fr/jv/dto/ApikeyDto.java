package fr.jv.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class ApikeyDto {

    @JsonProperty(index=1)
    private String valeurApikeys;

    @JsonProperty(index=2)
    private Integer quotaMensuel;


}