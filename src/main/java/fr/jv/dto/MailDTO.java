package fr.jv.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter




public class MailDTO {
    @JsonProperty
    private String subject;
    @JsonProperty
    private String content;
    @JsonProperty
    private String recipientEmail;

    @Override
    public String toString() {
        return "MailDTO{" +
                "subject='" + subject + '\'' +
                ", content='" + content + '\'' +
                ", recipientEmail='" + recipientEmail + '\'' +
                '}';
    }
}