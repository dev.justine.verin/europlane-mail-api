package fr.jv.restClient;

import fr.jv.dto.MailDTO;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

@RegisterRestClient(baseUri = "ServiceApikeyRemote")
@Path("/keys")


public interface ServiceApikeyRemote {

    @GET
    @Path("{apikey}/quota")
    Response getQuota(@PathParam("apikey") String apikey);



    @GET
    @Path("{apikey}/count")
    Response getCountMail(@PathParam("apikey") String apikey);

    @GET
    @Path("{apikey}/client")
    Response getClient(@PathParam("apikey") String apikey);



    @POST
    @Path("{apikey}")
    Response addMailHistory(@PathParam("apikey") String apikey, MailDTO mailDTO);



}