package fr.jv.security;


import fr.jv.dto.ApikeyDto;
import fr.jv.restClient.ServiceApikeyRemote;
import jakarta.inject.Inject;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.Provider;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jboss.resteasy.client.exception.ResteasyWebApplicationException;


@Provider
public class APIKeyFilter implements ContainerRequestFilter {
    @Inject
    @RestClient
    jakarta.inject.Provider<ServiceApikeyRemote> serviceApikeyRemote;

    @Override
    public void filter(ContainerRequestContext requestContext) {
        String key = requestContext.getHeaderString("API-KEY");
        System.out.println("API Key :" + key);
        if (key == null || key.isEmpty()) {
            requestContext.abortWith(Response.status(Response.Status.FORBIDDEN).build());

        }
        try {
            ApikeyDto apiKeyinfo = serviceApikeyRemote.get().getClient(key).readEntity(ApikeyDto.class);
            int mailCount = serviceApikeyRemote.get().getCountMail(key).readEntity(Integer.class);
            int quota = apiKeyinfo.getQuotaMensuel();
            if (mailCount == quota && quota != 0) {
                requestContext.abortWith(Response.status(403, "Quota mensuel dépassé").build());
            }
        } catch (ResteasyWebApplicationException exception) {
            System.out.println("Echec envoi requête au client REST");
            requestContext.abortWith(Response.status(Response.Status.NOT_FOUND).build());
        }

    }


}